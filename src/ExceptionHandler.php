<?php

namespace Vhall\ErrorHandler;

use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

interface ExceptionHandler
{

    /**
     * Report or log an exception.
     *
     * @param  Throwable  $e
     * @return void
     *
     */
    public function report(Throwable $e): void;

    /**
     * Determine if the exception should be reported.
     *
     * @param Throwable $e
     * @return bool
     */
    public function shouldReport(Throwable $e);

    /**
     * Render an exception into an HTTP response.
     *
     * @param Throwable                 $e
     *
     */
    public function renderHttpResponse(Throwable $e): bool;

    /**
     * Render an exception to the console.
     *
     * @param Throwable       $e
     * @return void
     */
    public function renderForConsole(Throwable $e): bool;

}
